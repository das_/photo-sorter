package main

import (
	"fmt"
	"github.com/rwcarlsen/goexif/exif"
	"github.com/sqweek/dialog"
	"io/ioutil"
	"os"
	"runtime"
	"strconv"
	"strings"
	"time"
)

var (
	InputPath string
	OutputPath string
)

func main() {
	o1, _ := dialog.Directory().Title("Zu sortierende Dateien").Browse()
	o2, _ := dialog.Directory().Title("Ausgabeverzeichnis").Browse()
	InputPath = o1 + getSeperator()
	OutputPath = o2 + getSeperator()
	files, err := ioutil.ReadDir(InputPath)
	if err != nil {
		fmt.Println("Angegebene Datei ist kein Pfad!")
		println(err)
		os.Exit(-1)
	}
	c := 0
	for _, v := range files {
		c++
		fmt.Println(v.Name() + " (" + strconv.Itoa(c) + " / " + strconv.Itoa(len(files)) + ")")
		year, month := getDate(v)
		o := OutputPath + year + "-" + month + getSeperator() + v.Name()
		if _, err := os.Stat(o); os.IsNotExist(err) {
			_ = os.Mkdir(strings.Replace(o, v.Name(), "", 1), 777)
		}
		copy(InputPath + v.Name(), o)
	}
	fmt.Println("Vorgang erfolgreich!")
	time.Sleep(10 * time.Second)
	os.Exit(0)
}

func getSeperator() string {
	if runtime.GOOS == "windows" {
		return "\\"
	}
	return "/"
}

func getDate(v os.FileInfo) (year string, month string) {
	f, err := os.Open(InputPath + v.Name())
	if err != nil {
		year = strconv.Itoa(v.ModTime().Year())
		month = strconv.Itoa(int(v.ModTime().Month()))
		fmt.Println("Fehler! " + v.Name())
		return year, month
	}
	defer f.Close()
	x, err := exif.Decode(f)
	if err != nil {
		year = strconv.Itoa(v.ModTime().Year())
		month = strconv.Itoa(int(v.ModTime().Month()))
		fmt.Println("Fehler! " + v.Name())
		return year, month
	}
	tm, err := x.DateTime()
	year = strconv.Itoa(tm.Year())
	month = strconv.Itoa(int(tm.Month()))
	return year, month
}

func copy(source, destination string) {
	input, err := ioutil.ReadFile(source)
	if err != nil {
		fmt.Println(err)
		return
	}

	err = ioutil.WriteFile(destination, input, 0777)
	if err != nil {
		fmt.Println("Fehler beim Erstellen von ", destination)
		fmt.Println(err)
		return
	}
}